<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'bio' => $faker->text($maxNbChars = 200),
        'rating' => $faker->numberBetween($min = 1, $max = 150),
        'photo' => $faker->imageUrl($width = 640, $height = 480)
    ];
});

