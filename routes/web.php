<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UsersController@index')->name('home');
Route::get('/edit/{id}', 'UsersController@doEdit');
Route::post('/edit/{id}', 'UsersController@edit');
Route::get('/delete/{id}', 'UsersController@destroy');
Route::get('/addRating/{id}', 'UsersController@addRating');


/*- By clicking '+1' - user rating increased by 1 (in database).
- By clicking on pagination button - go to that page of users list.
- By clicking on 'delete' - the user is deleted from the database.
- By clicking on 'save' - save the user data to the database.
- By clicking on 'edit' - go to edit user page.
- By clicking on 'back' - return on main page.*/


