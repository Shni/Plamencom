<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::latest()->paginate(9);
        return view('lists', compact('users'));
    }

    public function doEdit($id)
    {
        $user = User::find($id);
        return view('edit', compact('user'));
    }

    public function addRating($id)
    {
        User::where('id', '=', $id)->increment('rating', 1);
        return redirect()->home();
    }

    public function edit(UserRequest $request, $id)
    {

        User::where('id', '=', $id)
              ->update([
                'name' => $request['name'],
                'bio' => $request['bio'],
                'photo' => $request['photo']
              ]);
        return redirect()->home();
    }

    public function destroy($id)
    {
        User::destroy($id);
        return redirect()->home();
    }
}
