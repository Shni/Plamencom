<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Plamencom</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <style type="text/css">
            .col-sm-6{
                height: 700px;
            }
        </style>
    </head>
    <body>
        <div class="container">
             @yield('content')
        </div>

    </body>
</html>
