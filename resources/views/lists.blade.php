@extends('main')

@section('content')

    <div class="container">
        <div class="row">
            @foreach($users as $user)
                <div class="col-sm-6" >
                    <div class="thumbnail">
                        <img src="{{ $user->photo }}">
                        <div class="caption">
                            <h3> {{ $user->name }} </h3>
                            <p> {{ $user->bio }} </p>
                            <a href="/addRating/{{ $user->id }}" class="btn btn-primary" role="button">Raiting: {{ $user->rating }}</a>
                            <a href="/edit/{{ $user->id }}" class="btn btn-primary" role="button">Edit</a>
                            <a href="/delete/{{ $user->id }}" class="btn btn-default" role="button">Delete</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        {{ $users->links() }}
    </div>

@endsection