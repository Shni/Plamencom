@extends('main')

@section('content')
    <h1> Edit user </h1>
    <div class="row">
        <div class="col-sm-6">
            <div class="thumbnail">
                <img src="{{ $user->photo }}">
                <form method="post" action="/edit/{{ $user->id }}">
                    {{ csrf_field()  }}
                    <div class="caption">
                        <div class="form-group">
                            <label for="photo">Фотографія</label>
                            <input class="form-control" type="text" name="photo" value="{{ $user->photo }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input class="form-control" type="text" name="name" value="{{ $user->name }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Bio</label>
                            <textarea class="form-control" type="text" name="bio"
                                      rows="5">{{ $user->bio }} </textarea>
                        </div>
                        <div>
                            <a href="/" class="btn btn-default" role="button">Home</a>
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                    @include('errors.error')
                </form>

            </div>
        </div>
    </div>


@endsection